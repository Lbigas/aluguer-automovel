import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;
/**
 * Classe abstrata para todos os tipos de Utilizadores
 */
public abstract class Utilizador implements Serializable
{

    private String nif;
    private String email;
    private String nome;
    private String password;
    private String morada;
    private LocalDate nascimento;
    private List<Integer> avaliacoes;
    private double classificacao;


    public abstract Utilizador clone();

    /**
     * Construtor vazio da classe Utilizador
     */
    public Utilizador(){
        this.nif = "";
        this.email = "";
        this.nome = "";
        this.password = "";
        this.morada = "";
        this.nascimento = LocalDate.of(1900,01,01);
        this.avaliacoes = new ArrayList<>();
        this.classificacao = 0;

    }

    /**
     * Construtor parametrizado da classe Utilizador
     * @param nif O nif do utilizador
     * @param e O endereço de email do utilizador
     * @param n O nome do utilizador
     * @param p A password do utilizador
     * @param m A morada do utilizador
     * @param nasc A data de nascimento do utilizador
     * @param aval Lista de avaliações
     * @param classificação
     */

    public Utilizador(String nif,String e, String n, String p, String m, LocalDate nasc,
            List<Integer> aval,double classificacao){
        this.nif = nif;
        this.email = e;
        this.nome = n;
        this.password = p;
        this.morada = m;
        this.nascimento = nasc;
        this.avaliacoes = aval;
        this.classificacao = classificacao;

        // numUsers +=1;
    }

    /**
     * Construtor por cópia da classe Utilizador
     * @param user Um utilizador
     */

    public Utilizador(Utilizador user){
        this.nif = user.getNif();
        this.email = user.getEmail();
        this.nome = user.getNome();
        this.password = user.getPassword();
        this.morada = user.getMorada();
        this.nascimento = user.getDataNasc();
        this.avaliacoes = user.getAvaliacao();
        this.classificacao = user.getClassificacao();
        
    }



    // Definição dos métodos de instância

    /**
    * Obter o Nif do utilizador
    *@return O nif do utilizador
    */
    public String getNif(){
        return this.nif;
    }

    /**
    * Definir o nif do utilizador
    * @param s O nif a definir
    */
    public void setNif(String s){
        this.nif = s;
    }

    /**
     * Metodo para devolver o endereço de email de um utilizador
     * @return O e-mail do utilizador
     */
    public String getEmail(){
        return this.email;
    }

    /**
     * Metodo para devolver o nome de um utilizador
     * @return O nomo do utilizador
     */
    public String getNome(){
        return this.nome;
    }

    /**
     * Método para devolver a password de um utilizador
     * @return A password do utilizador
     */

    public String getPassword(){
        return this.password;
    }


    /**
     * Método para devolver a morada de um utilizador
     * @return A morada do utilizador
     */
    public String getMorada(){
        return this.morada;
    }

    /**
     * Método para devolver a data de nascimento de um utilizador
     * @return A data de nascimento do utilizador
     */
    public LocalDate getDataNasc(){
        return this.nascimento;
    }

    /**
     * Método para obter a lista de avaliacoes de um Proprietário
     *@return Uma lista de avaliações
     */

    public List<Integer> getAvaliacao(){
        List<Integer> res = new ArrayList<>();
        for(Integer a : this.avaliacoes){
            res.add(a);
        }
        return res;
    }

    /**
    * Adicionar uma avaliação à lista de avaliações
    *@param x Uma avaliação a adicionar
    */
    public void addAvaliacao(Integer x){
        this.avaliacoes.add(x);
    }


    /**
     * Método para obter a classificação média de um Proprietário
     *@return O valor da classificação média de um utilizador
     * O valor do acumulador inicia com o valor da classificação e não a 0, pois pode ter recebido classificação do ficheiro de carregamento inicial
     */
    public double getClassificacao(){
        double c = this.classificacao;
        if (this.avaliacoes.size() == 0) return 0;
        for(Integer p: this.avaliacoes){
            c += p;
        }
        if(c>100) c = 100;
        return c/(this.avaliacoes.size());
    }

    /**
     * Método para actualizar endereço de email de um utilizador
     * @param e O novo endereço de email
     */
    public void setEmail(String e){
        this.email = e;
    }

    /**
     * Método para actualizar o nome de um utilizador
     * @param n A string com o nome
     */

    public void setNome(String n){
        this.nome = n;
    }


    /**
     * Método para redefinir a password de um utilizador
     * @param p A nova password
     */
    public void setPassword(String p){
        this.password = p;
    }

    /**
     * Método para actualizar a morada de um utilizador
     *@param m A nova morada
     */

    public void setMorada(String m){
        this.morada = m;
    }

    /**
     * Método para corrigir a data de nascimento de um utilizador
     *@param data A data de nascimento
     */
    public void setNasc(LocalDate data){
        this.nascimento = data;
    }

    /**
    * Verificar se dois objectos são iguais
    * @return Valor boleano
    */

    public boolean equals(Object o){
        if(this == o) return true;

        if((o == null) || (this.getClass() != o.getClass()))
            return false;

        Utilizador user = (Utilizador) o;

        return (this.nif.equals(user.getNif()) && this.email.equals(user.getEmail()) && this.nome.equals(user.getNome()) &&
                this.password.equals(user.getPassword()) && this.morada.equals(user.getMorada()) 
                && user.getAvaliacao().equals(this.getAvaliacao())
                && this.nascimento.equals(user.getDataNasc())
                && this.getClassificacao() == user.getClassificacao() && this.avaliacoes.equals(user.getAvaliacao()));
    }

    /**
    * Obter a informação de um utilizador
    */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Nif: ").append(this.getNif()).append(" ,email: ").append(this.getEmail())
            .append(" Nome: ").append(this.getNome())
            .append(" Password: ").append(this.getPassword())
            .append(" Morada: ").append(this.getMorada())
            .append(" Data de Nascimento: ").append(this.getDataNasc());
        return sb.toString();
    }

}

