import java.time.LocalDate;
import java.io.Serializable;

/**
* Historico
* Classe que regista a informaçao relativa aos alugueres
*/

public abstract class Historico implements Serializable
{
    private LocalDate data;
    private double custoViagem;
    private double distViagem;

    // definição de métodos abstractos aqui

    public abstract Historico clone();


    /**
     * Construtor vazio da classe Historico
     *
     */
    public Historico(){
        this.data = LocalDate.of(1900,01,01);
        this.custoViagem = 0;
        this.distViagem = 0;
    }

    /**
     *Construtor parametrizado da classe Historico
     *@param date Data do aluguer
     *@param custo O custo do aluguer
     *@param distancia A distância da viagem
     */
    public Historico(LocalDate date, double custo, double distancia){
        this.data = date;
        this.custoViagem = custo;
        this.distViagem = distancia;
    }

    /**
     *Construtor por cópia da classe Historico
     *@param h Um histórico de alugueres
     */
    public Historico(Historico hist){
        this.data = hist.getData();
        this.custoViagem = hist.getCustoV();
        this.distViagem = hist.getDistV();

    }   

    /**
     * Método para obter a data do aluguer
     * @return Data do aluguer
     */
    public LocalDate getData(){
        return this.data;
    }

    /**
     * Método para obter o custo de um aluguer
     * @return Custo do aluguer
     */
    public double getCustoV(){
        return this.custoViagem;
    }

    /**
     * Método para obter a distância de viagem de um aluguer
     * @return A distância da viagem
     */
    public double getDistV(){
        return this.distViagem;
    }

    /**
     * Método para definir a data de um aluguer
     * @param date A data de um aluguer
     */

    public void setData(LocalDate date){
        this.data = date;
    }

    /**
     * Método para definir o custo de um aluguer
     * @param c Custo de aluguer
     */
    public void setCustoV(double c){
        this.custoViagem = c;
    }

    /**
     * Método para definir a distância de viagem de um aluguer
     * @param v Distância de viagem de um aluguer
     */
    public void setDistV(double v){
        this.distViagem = v;
    }

    //não faz sentido definir Sets aqui pois isto será dado por cada aluguer e não deve ser possivel alterar registos de historico

    /**
     * Método to String da classe Historico
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Aluguer na data ").append(this.getData()).append(" com um custo de ").append(this.getCustoV()).append(" ,distância ").append(this.getDistV());

        return sb.toString();
    }

    /**
     * Método equals da classe Historico
     */
    public boolean equals(Object o){
        if(this ==o) return true;
        if((o == null)|| (o.getClass() != this.getClass())) return false;

        Historico hist = (Historico) o;

        return (this.data.equals(hist.getData())
                && this.custoViagem == hist.getCustoV()
                && this.distViagem == hist.getDistV());
    }
}
