import java.util.ArrayList;
import java.util.List;
import java.time.LocalDate;
import java.io.Serializable;
/**
 * Write a description of class Cliente here.
 */
public class Cliente extends Utilizador implements Serializable
{
    private Localizacao posicao;
    private List <HistoricoCliente> historico;




    /**
     * Construtor vazio
     */
    public Cliente(){
        super();
        this.posicao = new Localizacao();
        this.historico = new ArrayList<>();
    }

    /**
     * Construtor parameterizado
     * @param nif O nif do cliente
     * @param mail O endereço de email do utilizador
     * @param name O nome do utilizador
     * @param pass A password do utilizador
     * @param adress A morada do utilizador
     * @param nascimento A data de nascimento do utilizador
     * @param aval Lista de avaliações
     * @param classif A classificação
     * @param pos A localização
     * @param hist Lista de histórico de alugueres
     * @return Um cliente
     * */
     
    public Cliente(String nif,String mail,String name,String pass,String adress,LocalDate nascimento,List<Integer> aval,double classif,Localizacao pos, List<HistoricoCliente> hist){

        super(nif,mail,name,pass,adress,nascimento,aval,classif);
        this.posicao = pos;
        this.historico = hist;
    }

    /**
     * Construtor por copia
     * @param c Um cliente
     */
    public Cliente(Cliente c){
        super(c);
        this.posicao = new Localizacao(c.getPosicao());
        setHistorico(c.getHistorico());
    }

    /**
     * Metodo clone
     * @return Objeto da classe Cliente igual ao Objeto que recebe o metodo
     */
    public Cliente clone(){
        return new Cliente(this);
    }

    /**
     * Metodo para obter a localização do cliente
     * @return Localização do cliente 
     */
    public Localizacao getPosicao(){
        return this.posicao;
    }

    
    /**
     * Calcula a taxa de idade para aplicar nos alugueres
     * Aplica-se uma taxa de 20% se a idade for menor do que 24, e 10% se a idade estiver entre [24,28[
     * @return O valor da taxa
     */
    public double getTaxaIdade(){
        double idade = this.getIdade();
        if(idade < 24)
            return 1.20;
        if(idade > 24 && idade < 28)
            return 1.10;
        else
            return 1;
    }

    /**
     * Metodo para obter o historico de alugueres de um cliente
     * @return Lista do historico do cliente
     */
    public List<HistoricoCliente> getHistorico(){
        List<HistoricoCliente> res = new ArrayList<>();
        for(HistoricoCliente s : this.historico){
            res.add(s.clone());
        }
        return res;
    }

    /**
     * Adicionar um aluguer ao histórico
     * @param hist A informação do aluguer a adicionar ao histórico
     */
    public void addHistorico(HistoricoCliente hist){
        this.historico.add(hist.clone());
    }

    /**
     * Metodo para alterar localizacao do cliente
     * @param pos Nova possicao a ser utilizada
     */
    public void setPosicao(Localizacao pos){
        this.posicao = new Localizacao(pos);
    }

    /**
     * Metodo para alterar a lista do historico
     * @param his Historico a ser inserido
     */
    public void setHistorico(List<HistoricoCliente> hist){
        //this.historico.clear();
        this.historico = new ArrayList<>();
        for(HistoricoCliente s : hist){
            this.historico.add(s.clone());
        }
    }

    /**
     * Calcular a idade do cliente com base na data de nascimento
     * @return A idade 
     */
    private double getIdade(){
        double k = LocalDate.now().getYear() - this.getDataNasc().getYear();
        if (LocalDate.now().getMonthValue() < this.getDataNasc().getMonthValue()) k --;
        if(LocalDate.now().getMonthValue() == this.getDataNasc().getMonthValue() && LocalDate.now().getDayOfMonth() <= this.getDataNasc().getDayOfMonth()) k--;
        return k;
    }

    /**
     * Calcular o total de Km de todos os alugueres efectuados
     * @return O número total de Km
     */
    public double getTotaldeKm(){
        double totais = 0;
        for(HistoricoCliente hist : this.historico){
            totais+= hist.getDistV();
        }
        return totais;
    }


    /**
     * Metodo equals
     * @param Objeto a ser comparado
     * @return Valor boleano
     */
    public boolean equals(Object o){
        if(this ==o) return true;
        if((o == null)|| (o.getClass() != this.getClass())) return false;

        Cliente c = (Cliente) o;

        return(super.equals(c) && this.posicao.equals(c.getPosicao()) && this.historico.equals(c.getHistorico()));

    }

    /**
     * Metodo toString
     * @return String que contem a informação do cliente
     */
    public String toString(){

        StringBuilder sb = new StringBuilder();
        sb.append("Localizacao: ").append(this.getPosicao())
            .append("Historico: ").append(this.getHistorico().toString());

        return super.toString() + sb.toString();
    }


}

