import java.util.List;
import java.util.ArrayList;

/**
 * VeiculoCombustao
 */
public abstract class VeiculoCombustao extends Veiculo
{

    private static final double taxaCombustivel = 1.2;

    //métodos abstractos para obrigar a definir nas subclasses;
    public abstract VeiculoCombustao clone();

    /**
     * Construtor vazio da classe VeiculoCombustao
     *
     */
    public VeiculoCombustao(){
        super();

    }
    
    /**
     * Construtor parametrizado
     * @param marca Marca do veiculo
     * @param mat Matricula do veiculo
     * @param nif O nif do proprietário
     * @param velo Velocidade do veiculo
     * @param price Preco do veiculo
     * @param pos A localização
     * @param historico Historico de alugueres
     * @param classificacao classificacao dos alugueres do veiculo
     * @param consumo O consumo por Km
     * @param autonomia A autonomia actual do veículo
     * @param autonomiaMax A autonomia máxima do veículo
     * @return Um veículo híbrido
     */
    public VeiculoCombustao(String marca, String matricula,String nif, double velo, double price,Localizacao pos,List<HistoricoVeiculo> hist, List<Integer> classif,double consumo,double aut,double autMax){
        super(marca,matricula,nif,velo,price,pos,hist,classif,consumo,aut,autMax);

    }

    /**
     * Construtor por cópia
     * @param car VeiculoCombustao a copiar
     */
    public VeiculoCombustao(VeiculoCombustao car){
        super(car);

    }

    /**
     * Obter a taxa de combustível de um veículo
     * @return O valor da taxa de combustível
     */
    public double getTaxa(){
        return this.taxaCombustivel;
    }


    public boolean equals (Object o){
        if(this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        VeiculoCombustao car = (VeiculoCombustao) o;

        return (super.equals(car) && car.getTaxa() == this.getTaxa());
    }
}
