import java.time.LocalDate;
import java.io.Serializable;
/**
 * Write a description of class HistoricoVeiculo here.
 */
public class HistoricoVeiculo extends Historico implements Serializable
{
    private double classificacao;
    private String cliente;
    
    /**
     * Construtor vazio
     * @return Um objecto da classe HistoricoVeiculo
     */
    public HistoricoVeiculo(){
        super();
        this.classificacao = 0;
        this.cliente = "";
    }
    
     /**
     * Construtor parametrizado
     * @param data A data do aluguer
     * @param custo O custo da viagem
     * @param dist A distância de viagem
     * @param clas A classificacao dos alugueres do veiculo
     * @param c1 O Nif do cliente
     * @return Um objecto da classe HistoricoVeiculo
     */
    public HistoricoVeiculo(LocalDate data, double custo, double dist,double clas, String cl){
        super(data,custo,dist);
        this.classificacao = clas;
        this.cliente = cl;
    }
    
    /**
     * Construtor por cópia
     * @param Um histórico a copiar
     * @return Um objecto da classe HistoricoVeiculo
     */
    public HistoricoVeiculo(HistoricoVeiculo h){
        super(h);
        this.classificacao = h.getClassificacao();
        this.cliente = h.getCliente();
    }

    /**
     * Obter o nif do cliente
     * @return O Nif do cliente
     */
    public String getCliente(){
        return this.cliente;
    }
    
    /**
     * Definir o cliente
     * @param nif O nif do cliente
     */
    public void SetCliente(String nif){
        this.cliente = nif;
    }
    
    /**
     * Obter a classificacao dos alugueres do veiculo
     * @return a classificacao
     */
    public double getClassificacao(){
        return this.classificacao;
    }

     /**
     * Definir a classificação dos alugueres do veiculo
     * @param c A classificação a inserir
     */
    public void setClassificacao(double c){
        this.classificacao = c;
    }
    
    /**
     * Clonar um objecto HistoricoVeiculo
     */
    public HistoricoVeiculo clone(){
        return new HistoricoVeiculo(this);
    }
    
    /**
     * Comparação de dois objectos da classe HistoricoVeiculo
     * @param o Um objecto HistoricoVeiculo
     * @return Valor boleano
     */
    public boolean equals(Object o){
        if(this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        HistoricoVeiculo h = (HistoricoVeiculo) o;

        return (super.equals(h) && this.cliente.equals(h.getCliente())
                && this.classificacao == h.getClassificacao());

    }
}
