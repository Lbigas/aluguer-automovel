package UmCarroJaExceptions;


/**
 * ProprietarioExisteException
 * Classe para tratamento da excepção no caso de já existir um proprietário registado com o mesmo NIF
 */
public class ProprietarioExisteException extends Exception
{
    
    /**
     * Construtor vazio
     */
    public ProprietarioExisteException()
    {
        super();
    }
    
    /**
     * Construtor parametrizado
     * @param msg A mensagem a exibir
     */
    public ProprietarioExisteException(String msg)
    {
        super(msg);
    }
    
}
