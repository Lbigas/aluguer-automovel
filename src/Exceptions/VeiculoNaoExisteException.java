package UmCarroJaExceptions;


/**
 * VeiculoNaoExisteException
 * Classe para tratamento da excepção no caso de ser pedida uma intervenção num veículo que não está registado
 */
public class VeiculoNaoExisteException extends Exception
{
    
    /**
     * Construtor vazio
     */
    public VeiculoNaoExisteException()
    {
        super();
    }

    /**
     * Construtor parametrizado
     * @param msg A mensagem a exibir
     */
    public VeiculoNaoExisteException(String msg)
    {
        super(msg);
    }
}
