package UmCarroJaExceptions;


/**
 * ClienteExisteException
 * Classe para tratamento da excepção no caso de ser pedida uma intervenção num cliente que não está registado
 */
public class ClienteNaoExisteException extends Exception
{
    
   /**
   * Construtor vazio
   */
    public ClienteNaoExisteException()
    {
        super();
    }

    /**
     * Construtor parametrizado
     * @param msg A mensagem a exibir
     */
    public ClienteNaoExisteException(String msg)
    {
        super(msg);
    }
}
