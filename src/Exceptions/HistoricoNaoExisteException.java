package UmCarroJaExceptions;


/**
 * HistoricoNaoExisteException
 * Classe para tratamento da excepção no caso de não existir histórico de alugueres
 */
public class HistoricoNaoExisteException extends Exception
{
    
    /**
     * Construtor vazio
     */
    public HistoricoNaoExisteException()
    {
        super();
    }
    
     /**
     * Construtor parametrizado
     * @param msg A mensagem a exibir
     */
    public HistoricoNaoExisteException(String msg)
    {
        super(msg);
    }
}
