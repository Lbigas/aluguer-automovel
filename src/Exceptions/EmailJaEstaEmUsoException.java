package UmCarroJaExceptions;


/**
 * ClienteExisteException
 * Classe para tratamento da excepção no caso de de se introduzir um e-mail já registado aquando do registo
 */
public class EmailJaEstaEmUsoException extends Exception
{
    
    /**
   * Construtor vazio
   */
    public EmailJaEstaEmUsoException()
    {
        super();
    }

    /**
     * Construtor parametrizado
     * @param msg A mensagem a exibir
     */
    public EmailJaEstaEmUsoException(String msg)
    {
        super(msg);
    }
}
