package UmCarroJaExceptions;


/**
 * ClienteExisteException
 * Classe para tratamento da excepção no caso de já existir um cliente registado com o mesmo NIF
 */
public class ClienteExisteException extends Exception
{
    
    /**
     * Construtor vazio
     */
    public ClienteExisteException()
    {
        super();
    }

    /**
     /**
     * Construtor parametrizado
     * @param msg A mensagem a exibir
     */
    public ClienteExisteException(String msg)
    {
        super(msg);
    }
}
