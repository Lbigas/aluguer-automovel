package UmCarroJaExceptions;


/**
 * ProprietarioNaoExisteException
* Classe para tratamento da excepção no caso de ser pedida uma intervenção num proprietário que não está registado
 */
public class ProprietarioNaoExisteException extends Exception
{
    
   /**
   * Construtor vazio
   */
   public ProprietarioNaoExisteException()
   {
       super();
   }

   /**
     * Construtor parametrizado
     * @param msg A mensagem a exibir
     */
   public ProprietarioNaoExisteException(String msg)
   {
       super(msg);
   }
}
