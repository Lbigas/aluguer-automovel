package UmCarroJaExceptions;


/**
 * VeiculoNaoExisteException
 * Classe para tratamento da excepção no caso de ser pedida uma intervenção num veículo que não está registado
 */
public class VeiculoSemAutonomiaException extends Exception
{
    
    /**
     * Construtor vazio
     */
    public VeiculoSemAutonomiaException()
    {
        super();
    }

    /**
     * Construtor parametrizado
     * @param msg A mensagem a exibir
     */
    public VeiculoSemAutonomiaException(String msg)
    {
        super(msg);
    }
}
