package UmCarroJaExceptions;


/**
 * VeiculoExisteException
 * Classe para tratamento da excepção no caso de já existir um veículo registado com a mesma matricula
 */
public class VeiculoExisteException extends Exception
{
    
    /**
     * Construtor vazio
     */
    public VeiculoExisteException()
    {
        super();
    }

    /**
     * Construtor parametrizado
     * @param msg A mensagem a exibir
     */
    public VeiculoExisteException(String msg)
    {
        super(msg);
    }
}
