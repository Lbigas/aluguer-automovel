import java.time.LocalDate;
import java.io.Serializable;
public class HistoricoProprietario extends Historico implements Serializable
{
    private String matricula;
    private String cliente;
    private double classificacao;
    
     /**
     * Construtor vazio
     * @return Um objecto da classe HistoricoProprietario
     */
    public HistoricoProprietario(){
        super();
        this.matricula = "";
        this.cliente = "";
        this.classificacao = 0;
    }
    
     /**
     * Construtor parametrizado
     * @param data A data do aluguer
     * @param custo O custo da viagem
     * @param dist A distância de viagem
     * @param mat A matrícula do veículo
     * @param c1 O nif do cliente
     * @param clas A classificacao 
     * @return Um objecto da classe HistoricoProprietario
     */
    public HistoricoProprietario(LocalDate data, double custo,double dist,String mat, String cl, double clas){
        super(data,custo,dist);
        this.matricula = mat;
        this.cliente = cl;
        this.classificacao = clas;
    }
    
    /**
     * Construtor por cópia
     * @param Um histórico a copiar
     * @return Um objecto da classe HistoricoProprietario
     */
    public HistoricoProprietario(HistoricoProprietario h){
        super(h);
        this.matricula = h.getMatricula();
        this.cliente = h.getCliente();
        this.classificacao = h.getClassificacao();
    }
    
    /**
     * Obter a matricula do veículo utilizado
     * @return A matricula do veículo
     */
    public String getMatricula(){
        return this.matricula;
    }
    
    /**
     * Obter o nif do cliente
     * @return O Nif do cliente
     */
    public String getCliente(){
        return this.cliente;
    }
    
    /**
     * Obter a classificacao do proprietario
     * @return A classificacao
     */
    public double getClassificacao(){
        return this.classificacao;
    }
    
    /**
     * Definir a matrícula do veículo
     * @param mat A matricula
     */
    public void setMatricula(String mat){
        this.matricula = mat;
    }
    
    /**
     * Definir o cliente
     * @param nif O nif do cliente
     */
    public void setCliente(String cl){
        this.cliente = cl;
    }
    
    /**
     * Definir a classificação atribuida ao proprietario
     * @param class A classificação a inserir
     */
    public void setClassificacao(double c){

        this.classificacao = c;
    }
    
    /**
     * Clonar um objecto HistoricoProprietario
     */
    public HistoricoProprietario clone(){
        return new HistoricoProprietario(this);
    }
    
    /**
     * Comparação de dois objectos da classe HistoricoVeiculo
     * @param o Um objecto HistoricoVeiculo
     * @return Valor boleano
     */
    public boolean equals(Object o){
        if(this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        HistoricoProprietario h = (HistoricoProprietario) o;

        return (super.equals(h) && this.matricula.equals(h.getMatricula())
                && this.cliente.equals(h.getCliente())
                && this.classificacao == h.getClassificacao());

    }
    
    /**
     * Informação geral sobre o objecto
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(" do veículo " + this.getMatricula() + ", cliente " + this.getCliente() + " e com classificação de " +this.getClassificacao() + "\n");
        return super.toString() + sb.toString();
    }

}
