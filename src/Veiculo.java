import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

/**
 * Definição da classe Veiculo e os seus construtores e métodos
 */
public abstract class Veiculo implements Serializable
{
    private String marca;
    private String matricula;
    private String nif;
    private double velocidadeMedia;
    private double precoKm;
    private Localizacao posicao;
    private double consumoKm;
    private double autonomia;
    private double autonomiaMax;
    private List<HistoricoVeiculo> historico;
    private List<Integer> classificacao;


    //métodos abstractos para obrigar a definir nas subclasses;
    
    public abstract Veiculo clone();
    public abstract double getTaxa();


    /**
     * Construtor vazio da classe Veiculo
     */
    public Veiculo(){
        this.marca = "";
        this.matricula = "";
        this.nif = "";
        this.velocidadeMedia = 0;
        this.precoKm = 100000; 
        this.posicao = new Localizacao();
        this.historico = new ArrayList<>();
        this.classificacao = new ArrayList<>();
        this.consumoKm = 0;
        this.autonomia = 0;
        this.autonomiaMax = 0;

    }    

    /**
     * Construtor parameterizado da classe Veiculo
     * @param marca Marca do veiculo
     * @param mat Matricula do veiculo
     * @param nif O nif do 
     * @param velo Velocidade do veiculo
     * @param price Preco do veiculo
     * @param pos A localização
     * @param historico Historico de alugueres
     * @param classificacao classificacao dos alugueres do veiculo
     * @param consumo O consumo por Km
     * @param autonomia A autonomia actual do veículo
     * @param autonomiaMax A autonomia máxima do veículo
     * @return Um veículo
     */
    public Veiculo(String marca, String mat,String nif,double velo,
            double price,Localizacao pos,List<HistoricoVeiculo> historico,List<Integer> classificacao,
            double consumo, double autonomia,double autonomiaMax){

        this.marca = marca;
        this.matricula = mat;
        this.nif = nif;
        this.velocidadeMedia = velo;
        this.precoKm = price;
        this.consumoKm = consumo;
        this.autonomia = autonomia;
        this.autonomiaMax = autonomiaMax;
        this.setPosicao(pos);
        this.setHistorico(historico);
        this.setClassificacao(classificacao);

    }

    /**
     * Construtor por copia da classe Veiculo
     * @param viatura O veículo a copiar
     */
    public Veiculo(Veiculo viatura){
        this.marca = viatura.getMarca();
        this.matricula = viatura.getMatricula();
        this.nif = viatura.getNif();
        this.velocidadeMedia = viatura.getVelocidadeMedia();
        this.precoKm = viatura.getPrecoKm();
        this.autonomiaMax = viatura.getAutonomiaMax();
        this.setPosicao(viatura.getPosicao());
        this.setHistorico(viatura.getHistorico());
        this.setClassificacao(viatura.getClassificacao());
        this.setAutonomia(viatura.getAutonomia());
        this.setAutonomiaMax(viatura.getAutonomiaMax());
        this.setConsumo(viatura.getConsumo());
    }

    /**
     * Método para definir o consumo por Km
     * @param cons O valor do consumo
     */
    public void setConsumo(double cons){
        this.consumoKm = cons;
    }
    
    /**
     * Método para obter o consumo por Km
     * @return O consumo por Km do veículo
     */
    public double getConsumo(){
        return this.consumoKm;
    }
    
    /**
     * Método para definir a autonomia de um veículo
     * @param auto O valor da autonomia
     */
    public void setAutonomia(double auto){
        this.autonomia = auto;
    }
    
    /**
     * Método para obter a autonomia do veículo
     * @return A autonomia do veículo
     */
    public double getAutonomia(){
        return this.autonomia;
    }

    /**
     * Método para obter a autonomia máxima do veículo
     * @return A autonomia máxima do veículo
     */
    public double getAutonomiaMax(){
        return this.autonomiaMax;
    }

    /**
     * Método para definir a autonomia máxima de um veículo
     * @param auto O valor da autonomia máxima
     */
    public void setAutonomiaMax(double x){
        this.autonomiaMax = x;
    }


    /**
     * Método para obter a marca do Veiculo
     * @return A marca do veículo
     */
    public String getMarca(){
        return this.marca;
    }
    
    /**
     * Método para obter a matricula de um Veiculo
     * @return A matricula do veículo
     */
    public String getMatricula(){
        return this.matricula;
    }

    /**
     * Retorna nif do proprietario do veiculo
     * @return String Nif do proprietario
     */
    public String getNif(){
        return this.nif;
    }

    /**
     * Método para obter a velocidade média de um Veiculo
     * @return A velocidade média
     */
    public double getVelocidadeMedia(){
        return this.velocidadeMedia;
    }


    /**
     * Método para obter o custo por km de um Veiculo
     * @return O preço por Km
     */
    public double getPrecoKm(){
        return this.precoKm;
    }

    /**
     * Metodo para obter o historico do Veiculo
     * @return O histórico de um veículo
     */
    public List<HistoricoVeiculo> getHistorico(){
        List<HistoricoVeiculo> res = new ArrayList<>();
        for (HistoricoVeiculo s : this.historico){
            res.add(s.clone());
        }
        return res;
    }


    /**
     * Método para obter a obter a lista de classificações de um veiculo 
     * @return A lista de classificações de um veículo
     */
    public List<Integer> getClassificacao(){
        List<Integer> res = new ArrayList<>();
        for(Integer x : this.classificacao){
            res.add(x);
        }
        return res;
    }
    
    /**
     * Método para obter a obter a classificação média de um veiculo 
     * @return A classificação média de um veículo
     */
    public double classifMedia(){
        double k = 0;
        List<HistoricoVeiculo> ret = this.getHistorico();
        if(ret.size() == 0) return k;
        for(HistoricoVeiculo h : ret){
            k += h.getClassificacao();
        }
        return k/ret.size();
    }

    /**
     * Método para obter o total de Km de um veiculo 
     * @return O número de Km total de todos os alugueres feitos
     */
    public double getTotaldeKmV(){
        double totais = 0;
        for(HistoricoVeiculo hist : this.historico){
            totais+= hist.getDistV();
        }
        return totais;
    }


    /**
     * Defenir marca do veiculo
     * @param a String que indica a marca do veiculo
     */
    public void setMarca(String a){
        this.marca = a;
    }

    /**
     * Definir matricula do veiculo
     * @param a String que indica a matricula
     */
    public void setMatricula(String a){
        this.matricula = a;
    }

    /**
     * Definir Nif do proprietario do veiculo
     * @param nif String que define nif do proprietario
     */
    public void setNif(String nif){
        this.nif = nif;
    }

    /**
     * Definir velocidade media do veiculo
     * @param a double que define a velocidade
     */
    public void setVelocidadeMedia(double a){
        this.velocidadeMedia = a;
    }

    /**
     * Definir preco por Km 
     * @param a Valor do preço por Km
     */
    public void setPrecoKm(double a){
        this.precoKm = a;
    }

    /**
     * Método para definir o historico de Historico de um Veiculo
     */
    public void setHistorico(List<HistoricoVeiculo> hist){
        this.historico = new ArrayList<>();
        for(HistoricoVeiculo s : hist){
            this.historico.add(s.clone());
        }
    }

    /**
     * Método para adicionar um aluguer ao historico de um Veiculo
     */
    public void addHistorico(HistoricoVeiculo h){
        this.historico.add(h.clone());
    }

    /**
     * Método para definir a classificação de um Veiculo
     */
    public void setClassificacao(List<Integer> classi){
        this.classificacao = new ArrayList<>();
        for(Integer x : classi){
            this.classificacao.add(x);
        }
    }

    /**
     * Método para definir a localização de um Veiculo
     * @param b A nova localização
     */
    public void setPosicao(Localizacao b){
        this.posicao = new Localizacao(b);
    }

    /**
     * Método para obter a localização de um Veiculo
     * @return A localização do veículo
     */
    public Localizacao getPosicao(){
        return new Localizacao(this.posicao.getX(),this.posicao.getY());
    }

    /**
     * Método para adicionar a classificação de um Veiculo após um aluguer
     * @param x O valor da classificação a adicionar
     */
    public void addAvaliacao(Integer x){
        this.classificacao.add(x);
    }

    /**
     * Método para adicionar um aluguer ao histórico de Historico de um Veiculo
     * @s Histórico a adicionar à lista de históricos
     */
    public void HistAddAluguer(HistoricoVeiculo s){
        this.historico.add(s.clone());
    }

    /**
    * Obter o valor total facturado pelo veiculo
     * @return O total facturado
     */
    public double ganhosFacturados(){
        double ret = 0;
        for(HistoricoVeiculo h : this.historico){
            ret+= h.getCustoV();
        }
        return ret;
    }

    /**
     * Metodo equals do Veiculo
     * @param o Objeto
     * @return boolean
     */
    public boolean equals(Object o){
        if(this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        Veiculo car = (Veiculo) o;

        return (car.getMarca().equals(this.getMarca())
                && car.getMatricula().equals(this.getMatricula()) 
                && car.getNif().equals(this.getNif())
                && car.getVelocidadeMedia() == this.getVelocidadeMedia() 
                && car.getPrecoKm() == this.getPrecoKm() 
                && car.getHistorico().equals(this.getHistorico()) 
                && car.getClassificacao() == this.getClassificacao() 
                && car.posicao.getX() == this.posicao.getX()
                && car.posicao.getY() == this.posicao.getY()
                && car.getAutonomia() == this.getAutonomia()
                && car.getConsumo() == this.getConsumo()
                && car.getAutonomiaMax() == this.getAutonomiaMax());

    }

    /**
     * Metodo toString
     * @return String descritora do veiculo
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Marca: ").append(this.getMarca()).append(" ,Matricula: ").append(this.getMatricula()).append(", velocidade Média : ").append(this.getVelocidadeMedia())
            .append(", preço por km: ").append(this.getPrecoKm()).append(",encontra-se na posicao: ").append(this.posicao.getX()).append(",").append(this.posicao.getY())
            .append(", Autonomia/AutonomiaMax: ").append(this.getAutonomia()).append(" / ").append(this.getAutonomiaMax());

        return sb.toString();
    }


}
