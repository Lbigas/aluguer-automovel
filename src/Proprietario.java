import java.util.ArrayList;
import java.util.List;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Iterator;

/**
 * Write a description of class Proprietario here.
 */
public class Proprietario extends Utilizador implements Serializable
{
    private List<Veiculo> veiculos;
    private List<HistoricoProprietario> historico;

    /**
     * Construtor vazio da classe Proprietário
     */
    public Proprietario(){
        super();
        this.veiculos = new ArrayList<>();
        this.historico = new ArrayList<>();
    }

    /**
     * Construtor parametrizado classe Proprietário
     * @param nif Nif
     * @param mail Email
     * @param name Nome 
     * @param pass Password
     * @param adress Morada
     * @param nascimento Data de nascimento
     * @aval Lista de avaliações
     * @classif Classificação
     * @veiculos Lista de veículos
     * @hist Histórico de alugueres
     *
     */
    public Proprietario(String nif,String mail,String name,String pass,String adress,
            LocalDate nascimento,List<Integer> aval,double classif, List<Veiculo> veiculos,
            List <HistoricoProprietario> hist){
        super(nif,mail,name,pass,adress,nascimento,aval,classif);
        setVeiculos(veiculos);
        setHistorico(hist);
    }


    /**
     * Construtor por cópia da classe Proprietário
     * @param Um proprietário
     */
    public Proprietario(Proprietario owner){
        super(owner);
        this.veiculos = owner.getVeiculos();
        this.historico = owner.getHistorico();
    }

    /**
     * Adicionar classificação a um veículo
     * @param mat Matricula do veículo
     * @clas O valor da classificação
     */
    public void addClassVeic(String mat, Integer clas){
        for(Veiculo v : veiculos){
            if(v.getMatricula().equals(mat)){
                v.addAvaliacao(clas);
                return;
            }
        }
    }
    
    
    public void abastecerVeiculos(){
        for(Veiculo v : veiculos){
            v.setAutonomia(v.getAutonomiaMax());
        }
    }


    /**
     * Método para obter os veículos de um Proprietário
     * @return Lista de veiculos do proprietario
     */

    public List<Veiculo> getVeiculos(){
        List<Veiculo> res = new ArrayList<>();
        for(Veiculo v : this.veiculos){
            res.add(v.clone());
        }
        return res;
    }



    /**
     * Método para obter o histórico de alugueres de um Proprietário
     *@return Uma lista com o histórico de alugueres
     */

    public List<HistoricoProprietario> getHistorico(){
        List<HistoricoProprietario> res = new ArrayList<>();
        for(HistoricoProprietario s : this.historico){
            res.add(s.clone());
        }
        return res;
    }

    /**
     * Adicionar um aluguer à lista de histórico de alugueres
     * @param hp Histórico a adicionar
     */
    public void addHistorico(HistoricoProprietario hp){
        List<HistoricoProprietario> ret = this.getHistorico();
        ret.add(hp.clone());
        setHistorico(ret);
    }

    /**
     * Adicionar um histórico de aluguer a um veículo
     * @param mat A matricula
     * @hv Histórico a adicionar
     * Redefine a autonomia do veículo reduzindo o valor da distância * preço por km
     */
    public void addHistVeiculo(String mat, HistoricoVeiculo hv){
        for(Veiculo v : veiculos){
            if(v.getMatricula().equals(mat)){
                v.setAutonomia(v.getAutonomia() - hv.getDistV());
                v.addHistorico(hv);
                return;
            }
        }
    }

    /**
     * Abastecer um veículo
     * @param v O veículo a abastecer
     * Coloca a autonomia actual no valor da autonomia máxima
     */
    public void abasteceVeiculo(Veiculo v){
        for(Veiculo c : this.veiculos){
            if(c.getMatricula().equals(v.getMatricula())){
                v.setAutonomia(v.getAutonomiaMax());
            }
        }
    }

    /**
     * Método para clonar um objecto da classe Proprietario
     * @return Uma copia do objeto
     */
    public Proprietario clone(){
        return new Proprietario(this);
    }


    /**
     * Método para definir uma lista de veículos de um Proprietario
     * @param lista Uma lista de veículos
     */
    public void setVeiculos(List<Veiculo> lista){
        this.veiculos = new ArrayList<>();
        for(Veiculo v : lista){
            this.veiculos.add(v.clone());
        }
    }


    /**
     * Método para definir o histórico de alugueres de um Proprietario
     * @param hist Um histórico de alugueres
     */
    public void setHistorico(List<HistoricoProprietario> hist){
        this.historico = new ArrayList<>();
        for(HistoricoProprietario s : hist){
            this.historico.add(s.clone());
        }
    }


    /**
     * Método para adicionar um Veiculo à lista de Veiculos
     * @param car Um veiculo
     */

    public void addVeiculo(Veiculo car){
        List<Veiculo> ret = this.getVeiculos();
        ret.add(car.clone());
        setVeiculos(ret);

    }

    /**
     * Remover um veiculo do proprietario
     * @param car Objeto da classe Veiculo
     */
    public int removeVeiculo(String mat){
        List<Veiculo> ret = this.getVeiculos(); Veiculo x = new CarroElectrico(); int res = 0;
        //Iterator it = ret.iterator();
        /*while(it.hasNext()){
            Veiculo v = (Veiculo) it.next();
            if(v.getMatricula().equals(mat))
                it.remove();
                
        }*/   
        for(Veiculo k : ret){
            if(k.getMatricula().equals(mat)){
                x = k;
                res = 1;
            }
        }
        ret.remove(x);
        setVeiculos(ret);
        return res;
    }


    /**
     * Verificar se dois objectos da classe Proprietario são iguais
     */

    public boolean equals(Object o){
        if(this ==o) return true;
        if((o == null)|| (o.getClass() != this.getClass())) return false;

        Proprietario owner = (Proprietario) o;

        return (super.equals(owner)
                && owner.getVeiculos().equals(this.getVeiculos())
                && owner.getHistorico().equals(this.getHistorico()));
    }

    /**
     * Obter a informação de um objecto
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("; Numero de veiculos ").append(this.veiculos.size());

        return super.toString() + sb.toString();
    }

}

