import java.util.List;
/**
 * CarroCombustao
 */
public class CarroCombustao extends VeiculoCombustao
{
    /**
     * Construtor vazio
     */
    public CarroCombustao(){
        super();
    }

    /**
     * Construtor parametrizado da classe VeiculoHibrido
     * @param marca Marca do veiculo
     * @param mat Matricula do veiculo
     * @param nif O nif do proprietário
     * @param velo Velocidade do veiculo
     * @param price Preco do veiculo
     * @param pos A localização
     * @param historico Historico de alugueres
     * @param classificacao classificacao dos alugueres do veiculo
     * @param consumo O consumo por Km
     * @param autonomia A autonomia actual do veículo
     * @param autonomiaMax A autonomia máxima do veículo
     * @return Um CarroCombustao
     */
    public CarroCombustao(String marca, String matricula,String nif, double velo, double price,Localizacao pos,List<HistoricoVeiculo> hist, List<Integer> classif,double consumo,double aut,double autMax){
        super(marca,matricula,nif,velo,price,pos,hist,classif,consumo,aut,autMax);
    }

    /**
     * Construtor por cópia
     * @param Um CarroCombustao a copiar
     * @return Um CarroCombustao
     */
    public CarroCombustao(CarroCombustao b){
        super(b);
    }

    /**
     * Clonar um objecto da classe CarroCombustao
     * @return Um CarroCombustao
     */
    public CarroCombustao clone(){
        return new CarroCombustao(this);
    }

    /**
     * Compararação de dois objectos CarroCombustao
     */
    public boolean equals(Object o){
        if(this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        CarroCombustao car = (CarroCombustao) o;

        return super.equals(car);
    }
    
    /**
     * Devolve a informação sobre um objecto
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Carro Combustão: ");
        return sb.toString() + super.toString();
    }
}

