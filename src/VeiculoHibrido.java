import java.util.List;
import java.util.ArrayList;

/**
 * VeiculoHibrido
 */
public abstract class VeiculoHibrido extends Veiculo
{
    private static final double taxaCombustivel = 1.10;

    //métodos abstractos para obrigar a definir nas subclasses;
    
    public abstract VeiculoHibrido clone();

    /**
     * Construtor vazio da classe VeiculoHibrido
     *
     */
    public VeiculoHibrido(){
        super();
    }

    /**
     * Construtor parametrizado da classe VeiculoHibrido
     * @param marca Marca do veiculo
     * @param mat Matricula do veiculo
     * @param nif O nif do proprietário
     * @param velo Velocidade do veiculo
     * @param price Preco do veiculo
     * @param pos A localização
     * @param historico Historico de alugueres
     * @param classificacao classificacao dos alugueres do veiculo
     * @param consumo O consumo por Km
     * @param autonomia A autonomia actual do veículo
     * @param autonomiaMax A autonomia máxima do veículo
     * @return Um veículo híbrido
     */
    public VeiculoHibrido(String marca, String matricula,String nif, double velo, double price,Localizacao pos,List<HistoricoVeiculo> hist, List<Integer> classif,double consumo,double aut,double autMax){
        super(marca,matricula,nif,velo,price,pos,hist,classif, consumo, aut,autMax);

    }
    
    /**
    *Construtor por cópia
    *@param car Veículo Hibrido a copiar
    */
    public VeiculoHibrido(VeiculoHibrido car){
        super(car);
    }

    /**
     * Obter a taxa de combustível de um veículo
     * @return O valor da taxa de combustível
     */
    public double getTaxa(){
        return this.taxaCombustivel;
    }

    /**
     * Comparar dois objectos da classe VeiculoHibrido
     */
    public boolean equals (Object o){
        if(this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        VeiculoHibrido car = (VeiculoHibrido) o;

        return (super.equals(car) && car.getTaxa() == this.getTaxa());
    }
}
