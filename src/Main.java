import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InvalidClassException;

/**
 * Main
 * Classe que inicia todo o programa
 * Invoca o carregamento do ficheiro gravado por OIS ("DataBase1")
 * Caso não consiga encontrar esse ficheiro ou ocorra algum erro de carregamento, 
 * carrega o ficheiro fornecido pelo docente
 */
public class Main
{
    /**
     * Método que inicia o programa
     */
    public static void main(String[] args) throws IOException,ClassNotFoundException, InvalidClassException{
        BaseDados database = new BaseDados(); 
        try{
            FileInputStream fis = new FileInputStream("DataBase1");
            ObjectInputStream ois = new ObjectInputStream(fis);
            database = (BaseDados) ois.readObject();
            ois.close();
        }
        catch(FileNotFoundException e){
            database.loadSaveFile();
        }
        catch(ClassNotFoundException e){
            database.loadSaveFile();
        }

        catch(InvalidClassException e){
            database.loadSaveFile();
        }
        


        Menu main_menu = new Menu(database);

        FileOutputStream fos = new FileOutputStream("DataBase1");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(database);
        oos.flush();
        oos.close();

    }


}


