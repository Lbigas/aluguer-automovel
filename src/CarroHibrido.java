import java.util.List;
/**
 * CarroHibrido
 */
public class CarroHibrido extends VeiculoHibrido
{
    /**
     * Construtor vazio
     */
    public CarroHibrido(){
        super();
    }
    
    /**
     * Construtor parametrizado da classe VeiculoHibrido
     * @param marca Marca do veiculo
     * @param mat Matricula do veiculo
     * @param nif O nif do proprietário
     * @param velo Velocidade do veiculo
     * @param price Preco do veiculo
     * @param pos A localização
     * @param historico Historico de alugueres
     * @param classificacao classificacao dos alugueres do veiculo
     * @param consumo O consumo por Km
     * @param autonomia A autonomia actual do veículo
     * @param autonomiaMax A autonomia máxima do veículo
     * @return Um Carro híbrido
     */
    public CarroHibrido(String marca, String matricula,String nif, double velo, double price,Localizacao pos,List<HistoricoVeiculo> hist, List<Integer> classif,double consumo,double aut,double autMax){
        super(marca,matricula,nif,velo,price,pos,hist,classif,consumo,aut,autMax);
    }

    /**
     * Construtor por cópia
     * @param Um CarroHibrido a copiar
     * @return Um CarroHibrido
     */
    public CarroHibrido(CarroHibrido b){
        super(b);
    }

    /**
     * Clonar um objecto da classe CarroHibrido
     * @return Um CarroHibrido
     */
    public CarroHibrido clone(){
        return new CarroHibrido(this);
    }

    /**
     * Compararação de dois objectos CarroHibrido
     */
    public boolean equals(Object o){
        if(this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        CarroHibrido car = (CarroHibrido) o;

        return super.equals(car);
    }
    
    /**
     * Devolve a informação sobre um objecto
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Carro Hibrido: ");
        return sb.toString() + super.toString();
    }
}

