import java.time.LocalDate;
import java.io.Serializable;
public class HistoricoCliente extends Historico implements Serializable
{
    private String matricula;
    private double classificacao;
    private String proprietario;
    
    /**
     * Construtor vazio
     * @return Um objecto da classe HistoricoCliente
     */
    public HistoricoCliente(){
        super();
        this.matricula = "";
        this.classificacao = 0;
        this.proprietario = "";
    }

    /**
     * Construtor parametrizado
     * @param data A data do aluguer
     * @param custo O custo da viagem
     * @param dist A distância de viagem
     * @param mat A matrícula do veículo
     * @param clas A classificação do cliente
     * @param prop Nif do proprietário
     * @return Um objecto da classe HistoricoCliente
     */
    public HistoricoCliente(LocalDate data, double custo, double dist,String mat, double clas, String prop){
        super(data,custo,dist);
        this.matricula = mat;
        this.classificacao = clas;
        this.proprietario = prop;
    }

    /**
     * Construtor por cópia
     * @param Um histórico a copiar
     * @return Um objecto da classe HistoricoCliente
     */
    public HistoricoCliente(HistoricoCliente h){
        super(h);
        this.matricula = h.getMatricula();
        this.classificacao = h.getClassificacao();
        this.proprietario = h.getProprietario();
    }

    /**
     * Obter a matricula do veículo utilizado
     * @return A matricula do veículo
     */
    public String getMatricula(){
        return this.matricula;
    }

    /**
     * Obter a classificação atribuida ao cliente
     * @return A classificação
     */
    public double getClassificacao(){
        return this.classificacao;
    }

    /**
     * Obter o nif do proprietário do veículo
     * @return o Nif do proprietário do veículo
     */
    public String getProprietario(){
        return this.proprietario;
    }

    /**
     * Definir a matrícula do veículo
     * @param mat A matricula
     */
    public void setMatricula(String mat){
        this.matricula = mat;
    }

    /**
     * Definir a classificação atribuida ao cliente
     * @param class A classificação a inserir
     */
    public void setClassificacao(double clas){
        if(clas < 0) clas = 0;
        this.classificacao = clas;
    }

    /**
     * Definir o proprietário do veículo utilizado
     * @prop O nif do proprietário
     */
    public void setProprietario(String prop){
        this.proprietario = prop;
    }

    /**
     * Clonar um objecto HistoricoCliente
     */
    public HistoricoCliente clone(){
        return new HistoricoCliente(this);
    }

    /**
     * Comparação de dois objectos da classe HistoricoCliente
     * @param o Um objecto HistoricoCliente
     * @return Valor boleano
     */
    public boolean equals(Object o){
        if(this == o) return true;
        if((o == null) || (this.getClass() != o.getClass()))
            return false;

        HistoricoCliente h = (HistoricoCliente) o;

        return(super.equals(h) && this.matricula.equals(h.getMatricula()) && this.classificacao == h.getClassificacao()
                && this.proprietario.equals(h.getProprietario()));
    }

    /**
     * Informação geral sobre o objecto
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\n").append("Matricula: ").append(this.getMatricula())
            .append(" Proprietario: ").append(this.getProprietario())
            .append(" Classificacao: ").append(this.getClassificacao()).append("\n");
        return super.toString() + sb.toString();
    }
}
