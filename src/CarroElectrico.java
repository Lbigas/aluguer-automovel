import java.util.List;
/**
 *CarroElectrico
 */
public class CarroElectrico extends VeiculoElectrico
{
    /**
     * Construtor vazio
     */
    public CarroElectrico(){
        super();
    }

    /**
     * Construtor parametrizado da classe VeiculoHibrido
     * @param marca Marca do veiculo
     * @param mat Matricula do veiculo
     * @param nif O nif do proprietário
     * @param velo Velocidade do veiculo
     * @param price Preco do veiculo
     * @param pos A localização
     * @param historico Historico de alugueres
     * @param classificacao classificacao dos alugueres do veiculo
     * @param consumo O consumo por Km
     * @param autonomia A autonomia actual do veículo
     * @param autonomiaMax A autonomia máxima do veículo
     * @return Um CarroElectrico
     */
    public CarroElectrico(String marca, String matricula,String nif, double velo, double price,Localizacao pos,List<HistoricoVeiculo> hist, List<Integer> classif,double consumo,double aut,double autMax){
        super(marca,matricula,nif,velo,price,pos,hist,classif,consumo,aut,autMax);
    }
    
    /**
     * Construtor por cópia
     * @param Um CarroElectrico a copiar
     * @return Um CarroElectrico
     */
    public CarroElectrico(CarroElectrico b){
        super(b);
    }

    /**
     * Clonar um objecto da classe CarroHibrido
     * @return Um CarroElectrico
     */
    public CarroElectrico clone(){
        return new CarroElectrico(this);
    }

    /**
     * Compararação de dois objectos CarroElectrico
     */
    public boolean equals(Object o){
        if(this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        CarroElectrico car = (CarroElectrico) o;

        return super.equals(car);
    }

    /**
     * Devolve a informação sobre um objecto
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Carro Electrico: ");
        return sb.toString() + super.toString();
    }
}

